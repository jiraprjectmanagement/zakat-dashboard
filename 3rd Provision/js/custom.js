AOS.init();

  // Get titles from the DOM
  var titleMain  = $("#commentanimatedHeading");
  var titleSubs  = titleMain.find("slick-active");
  
  if (titleMain.length) {
  
  titleMain.slick({
    autoplay: false,
    arrows: false,
    dots: false,
    slidesToShow: 5,
    // centerPadding: "10px",
    draggable: false,
    infinite: true,
    pauseOnHover: false,
    swipe: false,
    touchMove: false,
    vertical: true,
    speed: 1000,
    autoplaySpeed: 2000,
    useTransform: true,
    cssEase: 'cubic-bezier(0.645, 0.045, 0.355, 1.000)',
    adaptiveHeight: true,
    responsive: [
      {
        breakpoint: 1920,
        settings: {
          slidesToShow: 5,
        }
      },
      {
        breakpoint: 1605,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 1367,
        settings: {
          slidesToShow: 3,
        }
      }
    ]
  });
  
  // On init
  $(".slick-dupe").each(function(index, el) {
    $("#commentanimatedHeading").slick('slickAdd', "<div>" + el.innerHTML + "</div>");
  });
  
  // Manually refresh positioning of slick
  titleMain.slick('slickPlay');
  };
    

  $('.raised-amount-counter').easyPieChart({
    easing: false,
     barColor: '#461a55',
     rackColor: false,
     scaleColor: false,
     scaleLength: 3,
     lineCap: 'circle',
     lineWidth: '4',
     size: 87,
     trackColor: '#ccc'
 });



 var $post = $(".mock-screen");
setInterval(function(){
    $post.toggleClass("display");
}, 20000);

  setInterval(function(){ 
    // toggle the class every 
    $('.scroll-img-anim').addClass('active-anim');  
    $('.scroll-img-anim').removeClass('no-active-anim');  
    // setTimeout(function(){
    //   // toggle another class
    //   $('.scroll-img-anim').addClass('no-active-anim');  
    //   $('.scroll-img-anim').removeClass('active-anim');  
    // },10000)
 
 },10000);




//  Particle Animation
 
const Confettiful = function (el) {
  this.el = el;
  this.containerEl = null;

  this.confettiFrequency = 3;
  this.confettiColors = ['#EF2964', '#00C09D', '#2D87B0', '#48485E', '#EFFF1D'];
  this.confettiAnimations = ['slow', 'medium', 'fast'];

  this._setupElements();
  this._renderConfetti();
};

Confettiful.prototype._setupElements = function () {
  const containerEl = document.createElement('div');
  const elPosition = this.el.style.position;

  if (elPosition !== 'relative' || elPosition !== 'absolute') {
    this.el.style.position = 'relative';
  }

  containerEl.classList.add('confetti-container');

  this.el.appendChild(containerEl);

  this.containerEl = containerEl;
};

Confettiful.prototype._renderConfetti = function () {
  this.confettiInterval = setInterval(() => {
    const confettiEl = document.createElement('div');
    const confettiSize = Math.floor(Math.random() * 3) + 7 + 'px';
    const confettiBackground = this.confettiColors[Math.floor(Math.random() * this.confettiColors.length)];
    const confettiLeft = Math.floor(Math.random() * this.el.offsetWidth) + 'px';
    const confettiAnimation = this.confettiAnimations[Math.floor(Math.random() * this.confettiAnimations.length)];

    confettiEl.classList.add('confetti', 'confetti--animation-' + confettiAnimation);
    confettiEl.style.left = confettiLeft;
    confettiEl.style.width = confettiSize;
    confettiEl.style.height = confettiSize;
    confettiEl.style.backgroundColor = confettiBackground;

    confettiEl.removeTimeout = setTimeout(function () {
      confettiEl.parentNode.removeChild(confettiEl);
    }, 3000);

    this.containerEl.appendChild(confettiEl);
  }, 25);
};

window.confettiful = new Confettiful(document.querySelector('.js-container'));


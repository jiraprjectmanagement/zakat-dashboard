$(function () {
    var minVal = 1, maxVal = 20; // Set Max and Min values
      // Increase product quantity on cart page
        $(".increaseQty").on('click', function(){
                var $parentElm = $(this).parents(".qtySelector");
                $(this).addClass("clicked");
                setTimeout(function(){
                    $(".clicked").removeClass("clicked");
                },100);
                var value = $parentElm.find(".qtyValue").val();
                if (value < maxVal) {
                    value++;
                }
            $parentElm.find(".qtyValue").val(value);
            $('#totalPledgeAmount').html(addCommas(PledgeOptions.getTotalPledgeAmount()));
        });
         // Decrease product quantity on cart page
        $(".decreaseQty").on('click', function(){
            var $parentElm = $(this).parents(".qtySelector");
            $(this).addClass("clicked");
            setTimeout(function(){
                $(".clicked").removeClass("clicked");
            },100);
            var value = $parentElm.find(".qtyValue").val();
            if (value > 1) {
                value--;
            }
            $parentElm.find(".qtyValue").val(value);
            $('#totalPledgeAmount').html(addCommas(PledgeOptions.getTotalPledgeAmount()));
        });
        // Decrease product quantity on cart page
        
        //count dynamic id and appear counter box
        $('.list-of-support.slec-plane ul li').each( function(i){
            $(this).attr("id","suppo-item-" + i++);
        });

    $('.mainCat').on('change', function (e) {
        resetFormFields();
        if (this.id == 'redirect') {
            $('.options-list').fadeOut(500);
            redirecDonate();
        } else {
            $('.options-list').fadeOut(500);
            $('.options-list').fadeIn(500);
        }
    });
    $(".pledge-option").click(function (e) {
        $('#totalPledgeAmount').html(addCommas(PledgeOptions.getTotalPledgeAmount()));
        if ($(this).is(':checked')) {
            if (this.id) {
                $("#" + this.id + "_div").addClass('std-count-active');
                $("#" + this.id + "_divYears").addClass('std-count-active');
                if (this.id == 'other_amount') {
                    $("#" + this.id + "_no").val('').removeAttr('disabled');
                }
            }
        } else {
            if (this.id) {
                $("#" + this.id + "_div").removeClass('std-count-active');
                $("#" + this.id + "_divYears").removeClass('std-count-active');
                if (this.id == 'other_amount') {
                    $("#" + this.id + "_no").val('').attr('disabled', 'disabled');
                }
                else
                    $("#" + this.id + "_no").val(1);
            }
        }
    });
    $(".qtyValue").on("change paste keyup", function () {
    //.change(function (e) {// works on other amount case
        $('#totalPledgeAmount').html(addCommas(PledgeOptions.getTotalPledgeAmount()));
    });
        //count dynamic id and appear counter box
        
        // Hide Show Select Pldge btn 
        $('.btn-top input[type="radio"]').click(function(){
            $(".list-of-support.slec-plane ul li:not(.list-of-support.slec-plane ul li:last-child)").addClass("active-attr"); 
            $(".list-of-support.slec-plane ul li:not(.list-of-support.slec-plane ul li:last-child)").removeClass("notactive-attr"); 
            $(".list-of-support.slec-plane ul li:last-child").addClass("notactive-attr"); 
            $(".list-of-support.slec-plane ul li:last-child").addClass("active-attr"); 
        });
        $('.btn-bottom input[type="radio"]').click(function(){
            $(".list-of-support.slec-plane ul li:not(.list-of-support.slec-plane ul li:last-child)").removeClass("active-attr"); 
            $(".list-of-support.slec-plane ul li:not(.list-of-support.slec-plane ul li:last-child)").addClass("notactive-attr"); 
            $(".list-of-support.slec-plane ul li:last-child").removeClass("notactive-attr"); 
            $(".list-of-support.slec-plane ul li:last-child").addClass("active-attr"); 
        });
        // Hide Show Select Pldge btn 

    $('input[name="recognition"]').on("change", function () {
        console.log(this.id);
        $(this).is(':checked');
        var recognition = $('input[name="recognition"]:checked').val();
        console.log(recognition);
        if (this.id == 'rdInMemoryOf') {
            $('#divInNameOf').show();
        } else {
            $('#divInNameOf').hide();
        }
    });
    $('#btnPledge').on("click", function (e) {

        var flag = true;
        var category = "";
        var considerAsZakat = "";
        var organization = "";
        var firstName = "";
        var lastName = "";
        var phone = "";
        var email = "";
        var recognition = '';
        var address = "";
        var inTheNameOf = "";
        var totalAmount = 0;
        var selectedOptions = [];
        var datetime = $('#dateTime').val();
        $('#pledgeCategoryError').addClass('text-hide');
        $('#pledgeCategoryError').html('');
        $('#firstNameError').addClass('text-hide');
        $('#fullNameError').html('');
        $('#lastNameError').addClass('text-hide');
        $('#lastNameError').html('');
        $('#contactNoError').addClass('text-hide');
        $('#contactNoError').html('');
        $('#emailError').addClass('text-hide');
        $('#emailError').html('');
        $('#addressError').addClass('text-hide');
        $('#addressError').html('');
        $('#recognitionError').addClass('text-hide');
        $('#recognitionError').html('');

        totalAmount = PledgeOptions.getTotalPledgeAmount();
        if (totalAmount <= 0) {
            alert('Please select an option to donate.');
            document.getElementById('full_named_scholarship_opportunity').scrollIntoView(true);
            document.getElementById('full_named_scholarship_opportunity').focus();
            return;
        }

        selectedOptions = PledgeOptions.getSelectOptions();
        console.log(selectedOptions);

        category = $('input[name="pledgeCategory"]:checked').val();
        recognition = $('input[name="recognition"]:checked').val();
        considerAsZakat = '';
        firstName = $('#firstName').val();
        lastName = $('#lastName').val();
        phone = $('#phone').val();
        email = $('#email').val();
        if ($('#chkZakat').is(':checked'))
            considerAsZakat = $('#chkZakat').val();
        address = $('#address').val();
        inTheNameOf = $('#txtInNameOf').val();
        organization = $('#oragnization').val();

        if (category) {
            if (category.trim().length == 0) {
                $('#pledgeCategoryError').html('Please select an option');
                $('#pledgeCategoryError').removeClass('text-hide');
                document.getElementById('Need_based_scholarship').scrollIntoView(true);
                flag = false;
            }
        } else {
            $('#pledgeCategoryError').html('Please enter your First Name');
            $('#pledgeCategoryError').removeClass('text-hide');
            document.getElementById('Need_based_scholarship').scrollIntoView(true);
            flag = false;
        }
        if (firstName) {
            if (firstName.trim().length == 0) {
                $('#firstNameError').html('Please enter your First Name');
                $('#firstNameError').removeClass('text-hide');
                document.getElementById('name').scrollIntoView(true);
                document.getElementById('firstName').focus();
                flag = false;
            } else if (firstName.trim().length > 30) {
                $('#firstNameError').html('First Name shuold be less than 31 characters');
                $('#firstNameError').removeClass('text-hide');
                document.getElementById('firstName').scrollIntoView(true);
                document.getElementById('firstName').focus();
                flag = false;
            }
        } else {
            $('#firstNameError').html('Please enter your First Name');
            $('#firstNameError').removeClass('text-hide');
            document.getElementById('firstName').scrollIntoView(true);
            document.getElementById('firstName').focus();
            flag = false;
        }
        if (lastName) {
            if (lastName.trim().length == 0) {
                $('#lastNameError').html('Please enter your Last Name');
                $('#lastNameError').removeClass('text-hide');
                document.getElementById('lastName').scrollIntoView(true);
                document.getElementById('lastName').focus();
                flag = false;
            } else if (lastName.trim().length > 30) {
                $('#lastNameError').html('Last Name shuold be less than 31 characters');
                $('#lastNameError').removeClass('text-hide');
                document.getElementById('lastName').scrollIntoView(true);
                document.getElementById('lastName').focus();
                flag = false;
            }
        } else {
            $('#lastNameError').html('Please enter your Last Name');
            $('#lastNameError').removeClass('text-hide');
            document.getElementById('lastName').scrollIntoView(true);
            document.getElementById('lastName').focus();
            flag = false;
        }

        if (phone) {
            if (phone.trim().length == 0) {
                $('#contactNoError').html('Please enter contact no');
                $('#contactNoError').removeClass('text-hide');
                document.getElementById('phone').scrollIntoView(true);
                document.getElementById('phone').focus();
                flag = false;
            } else if (phone.trim().length > 14) {
                $('#contactNoError').html('Contact no length should be less than or equal to 14 digits only.');
                $('#contactNoError').removeClass('text-hide');
                document.getElementById('phone').scrollIntoView(true);
                document.getElementById('phone').focus();
                flag = false;
            } else {
                $('#contactNoError').html('');
                $('#contactNoError').addClass('text-hide');
            }
        } else {
            $('#contactNoError').html('Please enter contact no.');
            $('#contactNoError').removeClass('text-hide');
            document.getElementById('phone').scrollIntoView(true);
            document.getElementById('phone').focus();
            flag = false;
        }
        if (email) {
            if (email.trim().length > 0) {
                if (validateEmail(email.trim())) {
                    $('#emailError').html('');
                    $('#emailError').addClass('text-hide');
                    if (email.trim().length > 64) {
                        $('#emailError').html('Email length should be less than or equal to 64 characters');
                        $('#emailError').removeClass('text-hide');
                        document.getElementById('email').scrollIntoView(true);
                        document.getElementById('email').focus();
                        flag = false;
                    }
                } else {
                    $('#emailError').html('Please enter a valid email');
                    $('#emailError').removeClass('text-hide');
                    document.getElementById('email').scrollIntoView(true);
                    document.getElementById('email').focus();
                    flag = false;
                }
            } else {
                $('#emailError').html('Please enter an email');
                $('#emailError').removeClass('text-hide');
                document.getElementById('email').scrollIntoView(true);
                document.getElementById('email').focus();
                flag = false;
            }
        } else {
            $('#emailError').html('Please enter an email');
            $('#emailError').removeClass('text-hide');
            document.getElementById('email').scrollIntoView(true);
            document.getElementById('email').focus();
            flag = false;
        }

        //if (address) {
        //    if (address.trim().length == 0) {
        //        $('#addressError').html('Please enter address');
        //        $('#addressError').removeClass('text-hide');
        //        document.getElementById('address').scrollIntoView(true);
        //        document.getElementById('address').focus();
        //        flag = false;
        //    } else if (address.trim().length > 350) {
        //        $('#addressError').html('Address shuold be less than 350 characters');
        //        $('#addressError').removeClass('text-hide');
        //        document.getElementById('address').scrollIntoView(true);
        //        document.getElementById('address').focus();
        //        flag = false;
        //    } else {
        //        // hide error msg
        //        $('#addressError').html('').removeClass('text-hide');
        //    }
        //} else {
        //    $('#addressError').html('Please enter address');
        //    $('#addressError').removeClass('text-hide');
        //    document.getElementById('address').scrollIntoView(true);
        //    document.getElementById('address').focus();
        //    flag = false;
        //}

        if (recognition) {
            $('#recognitionError').html('').addClass('text-hide');
            if (recognition == 'In the name/memory of') {
                if (inTheNameOf) {
                    if (inTheNameOf.trim().length == 0) {
                        $('#recognitionError').html('Please enter the name other than the self');
                        $('#recognitionError').removeClass('text-hide');
                        document.getElementById('txtInNameOf').scrollIntoView(true);
                        document.getElementById('txtInNameOf').focus();
                        flag = false;
                    } else if (inTheNameOf.trim().length > 30) {
                        $('#recognitionError').html('In the name/memory of shuold be less than 31 characters');
                        $('#recognitionError').removeClass('text-hide');
                        document.getElementById('txtInNameOf').scrollIntoView(true);
                        document.getElementById('txtInNameOf').focus();
                        flag = false;
                    }
                } else {
                    $('#recognitionError').html('Please enter the name other than the self');
                    $('#recognitionError').removeClass('text-hide');
                    document.getElementById('txtInNameOf').scrollIntoView(true);
                    document.getElementById('txtInNameOf').focus();
                    flag = false;
                }
            }
        } else {
            $('#recognitionError').html('Please select the option');
            $('#recognitionError').removeClass('text-hide');
            document.getElementById('rdInMemoryOf').scrollIntoView(true);
            document.getElementById('rdInMemoryOf').focus();
            flag = false;
        }

        // data posting section
        var pledge = {
            'Pledges': selectedOptions, 'FirstName': capitalize(firstName), 'LastName': capitalize(lastName), 'PhoneNo': phone, 'Email': email, 'Address': address, 'InTheNameOf': inTheNameOf, 'TotalPledgeAmount': totalAmount, 'Category': category, 'ConsiderAsZakat': considerAsZakat, 'Recognition': recognition, 'Organization': organization
        };
        console.log(pledge);

        if (selectedOptions.length <= 0) {
            document.getElementById('full_named_scholarship_opportunity').scrollIntoView(true);
            document.getElementById('full_named_scholarship_opportunity').focus();
            alert('Please select an option to donate.');
            flag = false;
        }
        if (flag) {
            showLoader();
            $.post('/Support/Pledge', pledge, function (resp) {
                console.log(resp);
                if (resp) {
                    if (resp.Success) {
                        var link = $('#ThankYouLink');
                        if (link) {
                            redirect(resp.FirstName, resp.LastName, resp.PledgerId);
                            return false;
                        }
                        else
                            alert('Thank you for your contribution');
                        hideLoader();
                    }
                    else {
                        hideLoader();
                        alert(resp.Msg);
                        return false;
                    }
                }
            });
        }
        return false;
        e.preventDefault();
    });

    $("#btncmt").on('click', function (event) {
        debugger;
        $("#txtcomment").show();
        $("#btnPost").show();
        $("#btncmt").hide();
    });

    $("#btnPost").on('click', function (event) {
        debugger;

        var txt = $("#txtcomment").val();
        if (txt != "") {

            $("#message").hide();
            var Name = $("#hfdonrName").val();
            var Comment = txt;
            var PledgerId = $("#hfPledgerId").val();
            var data = { Name, Comment, PledgerId };
            $.post('/Support/PostComment', data, function (resp) {
                if (resp) {
                    if (resp.Success) {
                        alert('Thank you for your valuable comments.');
                        $("#hfdonrName").val(resp.DonorName);
                        $("#txtcomment").val("");
                        $("#txtcomment").hide();
                        $("#btnPost").hide();
                        $("#btncmt").show();
                    } else {
                        alert('Something went wrong! please try again.');
                    }
                } else {
                    alert('Something went wrong! please try again.');
                }
            });
        }
        else {
            $("#message").show();
        }
    });


    // default initialization
    $('#Merit_based_scholership').prop('checked', false);
    $('#Need_based_scholarship').prop('checked', false);
    $('#redirect').prop('checked', false);
    $('.options-list').fadeOut(500);
    hideLoader();
});
function redirecDonate() {
 //   var a = window.open("http://aghufus.azurewebsites.net/home");
    var a = window.open("https://huffundraisecampaing-test.azurewebsites.net/");
    
}
function addCommas(amount) {
    var result = "0";
    if (amount) {
        amount = amount.toString();
        var result = "";
        amount = amount.split("").reverse().join("");
        for (var i = 0; i < amount.length; i++) {
            result = result + amount[i];
            if (i === 2)
                result = result + ',';
            if (i === 5)
                result = result + ',';
            if (i === 8)
                result = result + ',';
            if (i === 11)
                result = result + ',';
            if (i === 14)
                result = result + ',';

            // console.log(result);
        }
        result = result.split("").reverse().join("");
        if (result.charAt(0) === ',')
            result = result.slice(1);
    }
    return result;
}
function onOtherTxtEvents(e) {
    if (e) {
        try {
            e.value = addCommas(e.value);
            var value = e.value.replace(/,/g, '');
            if (value > 999999) {
                alert('Amount should be between 0 - 999,999');
                e.value = 0;
               // $('#' + parentId).attr('data-amount', 0);
            }
        } catch (e) { alert('unable to remove commas from other amount'); }
    }
}
function capitalize(text) {
    return text.replace(/\b\w/g, function (m) { return m.toUpperCase(); });
}
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
jQuery('.numbersOnly').keyup(function () {
    this.value = this.value.replace(/[^0-9]/g, '');
});

jQuery('.numbersOnly').change(function () {
    this.value = this.value.replace(/[^0-9]/g, '');
});
jQuery('.numbersOnlyWithComma').change(function () {
    this.value = this.value.replace(/[^0-9]/g, '');
    this.value = addCommas(this.value);
});
jQuery('.numbersOnlyWithoutComma').change(function () {
    this.value = this.value.replace(/[^0-9]/g, '');
});
jQuery('.alphabateOnly').keyup(function () {
    this.value = this.value.replace(/[^a-zA-Z\ ]/g, '');
});
function validateEmail(email) {
    //var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if (!emailReg.test(email)) {
        return false;
    } else {
        return true;
    }
}
function redirect(firstName, lastName, pledgerId) {
    var url = ((document.location.origin + $('#ThankYouLink').val()) + "?FirstName=" + firstName + "&LastName=" + lastName + "&PledgerId=" + pledgerId);
    document.location.href = url;
}
function showLoader() {
    try {
        $("#mask").show();
        $("#loader").show();
    } catch (e) {
    }
}
function hideLoader() {
    try {
        $("#mask").hide();
        $("#loader").hide();
    } catch (e) {
    }
}
var PledgeOptions = {
    getTotalPledgeAmount: function () {
        var _pledgeOptions = $('.pledge-option');
        var _totalAmount = 0;
        if (_pledgeOptions) {
            if (_pledgeOptions.length > 0) {
                //console.log(_pledgeOptions.length);
                $.each(_pledgeOptions, function (index, data) {
                    try {
                        if (data) {
                            if ($(data).is(':checked')) {
                                var itemId = data.id;
                                var itemNo = $('#' + itemId + '_no').val();
                                var years = $('#' + itemId + '_years').val();

                                var itemValue = data.value;
                                if (itemValue) {
                                    var intItemValue = parseInt(itemValue);
                                    if (itemNo) {
                                        itemNo = itemNo.replace(/,/g, '');//remvoe commas in case of other amount
                                        var intItemNo = parseInt(itemNo);
                                        if (intItemNo > 0) {
                                            if (intItemValue > 0) {
                                                //    _totalAmount += (intItemValue * intItemNo);
                                                // adding years
                                                if (years) {
                                                    var intYears = parseInt(years);
                                                    if (intYears > 0) {
                                                        _totalAmount += (intItemValue * intYears * intItemNo);
                                                    }
                                                } else {
                                                    // for other amount case
                                                    _totalAmount += (intItemValue * intItemNo);
                                                }
                                            }
                                            //end of years
                                        } else {
                                            if (intItemValue > 1) {// 1 is to overcome other case
                                                _totalAmount += intItemValue;
                                            }
                                        }
                                    } else {// 1 is to overcome other case
                                        if (intItemValue > 1) {
                                            _totalAmount += intItemValue;
                                        }
                                    }
                                }
                            }
                        }
                    } catch (e) { console.log('error while getting total pledge amount'); console.log(e);}
                });
            }
        }
        
        //console.log(_totalAmount);
        return _totalAmount;
    },
    getSelectOptions: function () {
        var _pledgeOptions = $('.pledge-option');
        var _totalAmount = 0;
        var _category = $('input[name="pledgeCategory"]:checked').val();
        var _datetime = $('#dateTime').val();
        var _selectedOptions = [];
        if (_pledgeOptions) {
            if (_pledgeOptions.length > 0) {
                //console.log(_pledgeOptions.length);
                $.each(_pledgeOptions, function (index, data) {
                    try {
                        if (data) {
                            if ($(data).is(':checked')) {
                                var itemId = data.id;
                                var itemNo = $('#' + itemId + '_no').val();
                                var years = $('#' + itemId + '_years').val();
                                var label = $('#' + itemId + '_label').text();
                                var weightage = $('#' + itemId + '_weightage').val();
                                var subCateId = $('#' + itemId + '_id').val();
                                _totalAmount = 0;

                                var itemValue = data.value;
                                if (itemValue) {
                                    var intItemValue = parseInt(itemValue);
                                    if (itemNo) {
                                        itemNo = itemNo.replace(/,/g, '');//remvoe commas in case of other amount
                                        var intItemNo = parseInt(itemNo);
                                        if (intItemNo > 0) {
                                            if (intItemValue > 0) {
                                                //    _totalAmount += (intItemValue * intItemNo);
                                                // adding years
                                                if (years) {
                                                    var intYears = parseInt(years);
                                                    if (intYears > 0) {
                                                        _totalAmount += (intItemValue * intYears * intItemNo);
                                                        if (weightage) {
                                                            var intWeightage = parseFloat(weightage);
                                                            weightage = (intWeightage * intItemNo);
                                                        }
                                                        _selectedOptions.push({ 'Category': _category, 'SubCategory': label, 'Value': intItemValue, 'NoOfYears': intYears, 'NoOfStudents': intItemNo, 'SubCategoryPledgedAmount': _totalAmount, 'Weightage': weightage, 'SubCategoryId': subCateId, 'CreatedOn': _datetime, 'UpdatedOn': _datetime });
                                                    }
                                                } else {
                                                    // for other amount case
                                                    _totalAmount += (intItemValue * intItemNo);
                                                    _selectedOptions.push({ 'Category': _category, 'SubCategory': label, 'Value': intItemValue, 'NoOfYears': 0, 'NoOfStudents': intItemNo, 'SubCategoryPledgedAmount': _totalAmount, 'CreatedOn': _datetime, 'UpdatedOn': _datetime });
                                                }
                                            }
                                            //end of years
                                        } else {
                                            if (intItemValue > 1) {// 1 is to overcome other case
                                                _totalAmount += intItemValue;
                                            }
                                        }
                                    } else {// 1 is to overcome other case
                                        if (intItemValue > 1) {
                                            _totalAmount += intItemValue;
                                        }
                                    }
                                }
                            }
                        }
                    } catch (e) { console.log('error while getting total pledge amount'); console.log(e); }
                });
            }
        }

        //console.log(_totalAmount);
        return _selectedOptions;
    }
};

function resetFormFields() {
    try {
        $('.pledge-option').prop('checked', false);
        $('.radio_cus_pledg').prop('checked', false);
        $("input:text").val("");
        $("#totalPledgeAmount").html('');
        $('.qtyValue').val(1);
        $('.std-count').removeClass('std-count-active');
        $('#other_amount_no').val('');
    }
    catch (e) { }
    return false;
}

window.onpopstate = function () {
    //alert("clicked back button");
    //window.location.reload();
    window.location = document.referrer;
}; history.pushState({}, '');
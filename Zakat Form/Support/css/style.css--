@import url('https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700;800&display=swap');

html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed, 
figure, figcaption, footer, header, hgroup, 
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
	margin: 0;
	padding: 0;
	border: 0;
	font-size: 100%;
	font: inherit;
	vertical-align: baseline;
}
/* HTML5 display-role reset for older browsers */
article, aside, details, figcaption, figure, 
footer, header, hgroup, menu, nav, section {
	display: block;
}
html{
    
    scroll-behavior: smooth;
}
body {
	line-height: 1;
    font-family: 'Poppins', sans-serif !important;
    scroll-behavior: smooth;
}
ol, ul {
	list-style: none;
}
blockquote, q {
	quotes: none;
}
blockquote:before, blockquote:after,
q:before, q:after {
	content: '';
	content: none;
}
table {
	border-collapse: collapse;
	border-spacing: 0;
}
a:hover{
    text-decoration: none !important;
}
@media(min-width:1200px){
    .container{
      max-width: 1170px !important;
  }
}
h1{
  font-size: 18px;
  font-weight: 500;
}
h2{
  font-size: 20px;
  font-weight: 700;
}
h3{
  font-size: 15px;
  font-weight: 500;
}
h1,h2,h3{
  margin: 0;
  color: #5f2567;
}
p{
  font-size: 14px;
  color: #5f2567;
}

/* Header */
.header-content {
  text-align: center;
}
.header-content h1 {
  margin-bottom: 10px;
}
header {
  background-image: url(../img/header-shade.png);
  padding-top: 30px;
  position: relative;
  background-position: top center;
  background-repeat: repeat-x;
}

header:before {
  content: '';
  position: absolute;
  top: 0;
  background-image: url(../img/header-top-left.svg);
  background-repeat: no-repeat;
  background-position: top left;
  width: 100%;
  height: 100%;
  pointer-events: none;
  background-size: 8%;
}

.logo {
    display: flex;
    justify-content: center;
    padding-bottom: 15px;
}

.logo img.logo-main {
    margin-right: 27px;
}

/* Header */

/* Pledge Area */
section {
  padding: 10px 0;
}
h2.sec-heading:after {
  content: '';
  background: rgb(237 189 93);
  height: 4px;
  width: 43%;
  position: absolute;
  bottom: -7px;
  left: 0;
}
.pledge-area h2.sec-heading{
  text-align: center;
}
.pledge-area h2.sec-heading:after{
  display: none;
}
h2.sec-heading {
    position: relative;
}
.wrapper{margin: 10px 0px 0;}

.btn-top label {
    width: 50%;
}
.wrapper .option{
  background: #fff;
  height: 100%;
  margin: 0 3px;
  border-radius: 4px;
  cursor: pointer;
  border: 1px solid #888;
  transition: all 0.3s ease;
  padding: 11px 12px;
}
.btn-top {
    display: flex;
    justify-content: center;
}
.btn-bottom {
    margin-top: 15px;
}
.wrapper input[type="radio"]{
  display: none;
}
.checklist-left {
    display: flex;
    margin-bottom: 4px;
    position: relative;
    font-weight: 500;
}
.no-std{
  font-size: 10px;
  color: #5f2567;
  font-weight: 600;
}
#option-1:checked:checked ~ .option-1,
#option-2:checked:checked ~ .option-2,
#option-3:checked:checked ~ .option-3,
#option-4:checked:checked ~ .option-4,
#option-5:checked:checked ~ .option-5{
  border-color: #5f2567;
  box-shadow: 0 0 7px 0 #ccc;
}
.wrapper .option span{
  font-size: 12px;
  color: #888;
}
span.iner-span {
    display: block;
    font-size: 9.5px !important;
    font-weight: 600;
}
#option-1:checked:checked ~ .option-1 span,
#option-2:checked:checked ~ .option-2 span,
#option-3:checked:checked ~ .option-3 span,
#option-4:checked:checked ~ .option-4 span,
#option-5:checked:checked ~ .option-5 span{
  color: #5f2567;
  font-weight: 500;
}

#option-1:checked:checked ~ .option-1 img,
#option-2:checked:checked ~ .option-2 img,
#option-3:checked:checked ~ .option-3 img,
#option-4:checked:checked ~ .option-4 img,
#option-5:checked:checked ~ .option-5 img{
  filter: invert(4%) sepia(30%) saturate(3171%) hue-rotate(245deg) brightness(83%) contrast(83%);
}


/* Radio Button Style */


.radio-custom {
  opacity: 0;
  position: absolute;   
}

.radio-custom, .radio-custom-label {
  display: inline-block;
  vertical-align: middle;
  margin: 5px;
  cursor: pointer;
}

.radio-custom-label {
  position: relative;
}

.radio-custom + .radio-custom-label:before {
  content: '';
  background: #fff;
  border: 2px solid #ddd;
  display: inline-block;
  vertical-align: middle;
  width: 23px;
  height: 23px;
  margin-right: 10px;
  text-align: center;
}



.radio-custom + .radio-custom-label:before {
  border-radius: 50%;
}

.radio-custom:checked + .radio-custom-label:before {
  content: "\f00c";
  font-family: 'FontAwesome';
  color: #bbb;
  font-size: 13px;
}
p.genra-span {
    margin-left: 32px;
    margin-bottom: 0;
    color: #000;
    font-size: 13px;
    font-weight: 400;
}

label.radio-custom-label.redirect-url {
}
label.radio-custom-label {
  background: #fff;
  margin: 0 3px;
  border-radius: 4px;
  cursor: pointer;
  border: 1px solid#ddd;
  transition: all 0.3s ease;
  margin-bottom: 8px;
  width: 100%;
  padding: 12px 6%;
  font-weight: 500;
}
a.redirect-url {
  width: 100%;
  height: 100%;
  position: absolute;
}

.radio-row.redirect-row {
  position: relative;
}
.radio-custom:checked:checked ~ .radio-custom-label {
  border-color: #5f2567;
  box-shadow: 0 0 7px 0 #ccc;
  color: #5f2567;
}
.radio-custom:checked:checked ~ .radio-custom-label p {
  color: #5f2567;
}
.radio-row input[type=checkbox], input[type=radio] {
  display: none;
}
.radio-custom:checked:checked ~ .radio-custom-label:before{
  border-color: #5f2567;
  color: #5f2567;
}





/*  Radio Button Style  */

.option  img {
    margin-right: 5px;
}
.btn-wraper h3{
  text-align: center;
}
.btn-wraper {
    margin-top: 20px;
}
.sec-brder {
  border-top: 1px solid #e5cece;
}
.btn-wraper.sec-brder {
  padding-top: 15px;
}
.qtySelector{
	border: 1px solid #612668;
	width: 87px;
	height: 26px;
	color: #612668;
	border-radius: 4px;
}
.qtySelector .fa{
	padding: 10px 4px;
	width: 25px;
	height: 100%;
	float: left;
	cursor: pointer;
	font-size: 8px;
	background: #f5e9f7;
}
/* .qtySelector .fa.clicked{
	font-size: 12px;
	padding: 12px 5px;
} */
.qtySelector .qtyValue{
	border: none;
	padding: 5px;
	width: 35px;
	height: 100%;
	float: left;
	text-align: center;
	font-size: 12px;
}
.list-of-support ul li {
    padding: 15px 0;
    display: block;
    justify-content: space-between;
    border-bottom: 1px solid #ccc;
    position: relative;
}

.list-of-support.slec-plane ul li:last-child {
  /* border-bottom: transparent; */
}

.list-of-support.slec-plane ul li:last-child {
    /* border-bottom: transparent; */
}
.list-of-support ul li label {
    margin: 0;
    font-size: 13px;
    color: #612668;
    margin-left: 8px;
}
.decreaseQty {
    border-radius: 4px 0 0 4px;
}
.user-am-set {
  box-shadow: 0 0 11px 0 #cccccc8c;
  border-color: #5f2567 !important;
}

.user-am-set input.form-control {
  border-color: #5f2567;
}

.user-am-set span#basic-addon1 {
  border-color: #5f2567;
  color: #5f2567 !important;
}

.user-am-set.input-group span#basic-addon1:after {
  background: #5f2567 !important;
}
.increaseQty {
    border-radius: 0 4px 4px 0;
}
.usd-price{
  font-size: 13px;
  font-weight: 500;
  margin-left: 27px;
}
.item-tick {
    align-items: center;
    padding-bottom: 5px;
    justify-content: space-between;
    position: relative;
}
.pledge-confrim-inner .list-of-support ul li .item-tick {
  display: flex;
  align-items: center;
}

.list-of-support.slec-plane ul li:last-child .item-tick {
  display: flex;
  align-items: center;
}
.s-list-right {
    margin-top: 0;
    text-align: center;
}
.second-screen .s-list-right .input-group {
  width: 100%;
}
.s-list-right .input-group {
  width: 127px;
}
.input-group.user-am-input {
    pointer-events: none;
}
.input-group.user-am-input.user-am-set {
    pointer-events: initial;
}
.second-screen .s-list-right {
    width: 88%;
    margin: 0 auto;
}
.s-list-right .input-group span#basic-addon1 {
    padding: 0 8px;
    font-size: 12px;
    height: 40px;
    background: no-repeat;
    position: relative;
    color: #b3b3b3;
    border-right: none;
}

.s-list-right .input-group input.form-control {
    height: 40px;
    border-left: none;
    background: no-repeat;
}
.s-list-right .input-group input::placeholder{
  font-size: 12px;
}
.s-list-right .input-group span#basic-addon1:after {content: '';position: absolute;top: 50%;height: 68%;width: 1px;background: #ced4da;right: 0;transform: translateY(-50%);}
#another-am:checked .user-am{
  display: none;
}
.user-am-input-active{
  box-shadow: rgb(204 204 204 / 67%) 0px 0px 7px 0px;
}
.list-of-support ul {
  margin-bottom: 0;
}
/* Pledge Area */




/* Pledge Confrim Area */
.pledgec-confirm h2.sec-heading:after {
  content: '';
  background: rgb(237 189 93);
  height: 4px;
  width: 63%;
  position: absolute;
  bottom: -7px;
  left: 0;
}
.pledgec-confirm h2.sec-heading{
  text-align: left;
}
.pledge-confrim-inner {
  /* border-bottom: 4px solid #e9e9e9; */
  padding-top: 0px;
  /* border-top: 4px solid #e9e9e9; */
}
.p-conf-right span.pledge-con-amount {
    font-weight: 700;
    display: block;
}

.pledge-con-head {
    display: flex;
    justify-content: space-between;
}

.p-conf-right {
    text-align: right;
}
/* Pledge Confrim Area */

/* Detail Form */
.donation-detailo-form .form-group input{
  height: 43px;
  width: 100%;
  border: 1px solid #88888894;
  border-radius: 5px;
  position: relative;
  padding: 0 33px;
}
.form-control:focus {
    color: #495057;
    background-color: #fff;
    border-color: #5f2567 !important;
    outline: 0;
    box-shadow: 0 0 11px 0 #cccccc8c !important;
}
.donation-detailo-form .form-group{
  position: relative;
}
img.icon-control {
    position: absolute;
    top: 31%;
    z-index: 999;
    left: 10px;
    transform: translateY(-50%);
}
.donation-detailo-form .form-group input::placeholder{
  font-size: 13px;
  color:#88888894;
}
.detail-form-inner p {
  text-align: center;
  padding: 0 6px;
}
.detail-form-inner {
  /* border-bottom: 4px solid #e9e9e9; */
}

/* Detail Form */


/* Pldge Recognize */
.pledge-recognized .list-of-support ul li {
  padding: 10px 0;
}
.list-of-support ul li:last-child {
    /* border-color: transparent; */
    display: flex !important;
}
.pledge-inner-recognize .item-tick {
  display: flex;
  align-items: center;
  justify-content: flex-start;
}
.pledge-inner-recognize .list-of-support ul li:last-child {
    border-bottom: transparent;
}
button.pledge-btn {
  background: #5f2567;
  color: #fff;
  border-radius: 4px;
  border: none;
  width: 100%;
  height: 43px;
  box-shadow: 0 0 12px 0px #ccc;
}
input#btnPost.pledge-btn {
  background: #5f2567;
  color: #fff;
  border-radius: 4px;
  border: none;
  width: 100%;
  height: 43px;
  box-shadow: 0 0 12px 0px #ccc;
}
.std-count.std-count-active {
    opacity: 1;
    pointer-events: auto;
    filter: initial;
}
.std-count {opacity: 0.5;pointer-events: none;filter: grayscale(100%);display: flex;}

.list-of-support.slec-plane .s-list-right {
    display: flex;
}

.years-count {
    margin-left: 27px;
}
.list-of-support.slec-plane .s-list-right .std-count {flex-direction: column;}
/* Pldge Recognize */

/* Footer Content */
.footercon-iner p{
 text-align: center;
 font-size: 12px;
 color: #b3b3b3;
 padding: 0 8%;
 margin: 0;
}
.footercon-iner span{
  font-weight: 600;
  color:#295991;
  display: block;
  font-size: 14px;
}


.footer-cottom-link p {
    font-size: 14px;
    padding: 0;
}

.footer-cottom-link a {
    color: #b3b3b3;
    font-size: 13px;
}

.footer-cottom-link {
    text-align: center;
    padding-bottom: 61px;
    padding-top: 10px;
}
.main-footer{
  background-image: url(../img/footer-shade.png);
  overflow: hidden;
  background-repeat: repeat-x;
  background-position: bottom center;
  padding: 60px 0;
  position: fixed;
  bottom: 0;
  width: 100%;
  pointer-events: none;
}
.main-footer:after {
  content: '';
  position: absolute;
  top: 0;
  background-image: url(../img/footer-bottom-right.svg);
  background-repeat: no-repeat;
  background-position: bottom right;
  width: 100%;
  height: 100%;
  pointer-events: none;
  background-size: 8%;
  right: 0;
}
/* Thank You Page */
.thank-u-banner{
  background: url(../img/thankyou-banner.png);
  background-repeat: no-repeat;
  background-position: center;
  padding: 69px 0;
}
.thnk-iner h1 {
    font-size: 44px;
    font-weight: 700;
    color: #fff;
    text-align: center;
}
.thak-form-wrap h2.sec-heading span {
  font-weight: 400;
  font-size: 14px;
}
.thak-form-wrap p {
  padding: 24px 0 0;
}
.tahbk-heding span {
  font-weight: 700;
}

.tahbk-heding {
  font-weight: 400;
  border-top: 1px solid #e9e9e9;
  padding-top: 15px;
  text-align: center;
}
.donation-detailo-form .form-group textarea{
  height: 139px;
  width: 100%;
  border: 1px solid #88888894;
  border-radius: 5px;
  padding: 11px 11px;
}
.donation-detailo-form .form-group textarea::placeholder{
  font-size: 13px;
  color:#88888894;
}
input#txtcomment.donation-detailo-form .form-group textarea{
  height: 139px;
  width: 100%;
  border: 1px solid #88888894;
  border-radius: 5px;
  padding: 11px 11px;
}
input#txtcomment.donation-detailo-form .form-group textarea::placeholder{
  font-size: 13px;
  color:#88888894;
}
.thak-form-wrap form.donation-detailo-form {
  margin-top: 26px;
}
.social-content {
  text-align: center;
}
.social-content {
  text-align: center;
  border-top: 1px solid #e9e9e9;
  border-bottom: 1px solid #e9e9e9;
  padding: 14px 0;
}

ul.social-tahnks {
  display: flex;
  justify-content: center;
  margin: 19px 0 30px;
}

ul.social-tahnks li {
  margin-right: 29px;
}

ul.social-tahnks li:last-child {
  margin-right: 0;
}
.social-content .bck-home {
  margin: 24px auto 10px;
  background: #5f2567;
  color: #fff;
  border-radius: 4px;
  border: none;
  width: 100%;
  height: 43px;
  box-shadow: 0 0 12px 0px #ccc;
  display: flex;
  align-items: center;
  justify-content: center;
}

span.chrcter-limit {
    text-align: right;
    display: block;
    font-size: 13px;
    padding-top: 8px;
    color: #5d2568;
	padding-bottom: 12px;
}

input.txtComment {
    margin-bottom: 10px;  padding: 20px 5px 50px 5px;}
a.bck-home {
  margin: 24px auto 10px;
  background: #5f2567;
  color: #fff;
  border-radius: 4px;
  border: none;
  width: 100%;
  height: 43px;
  box-shadow: 0 0 12px 0px #ccc;
  display: flex;
  align-items: center;
  justify-content: center;
}
.gren-tick {
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 12px;
}

.gren-tick h4 {
  margin-top: 10px;
  font-weight: 700;
  font-size: 19px !important;
  color: #5f2567;
}
button.pledge-btn-tp {
  background: transparent;
  color: #5f2567;
  border-radius: 4px;
  width: 100%;
  height: 43px;
  border: 1px solid #5f2567;
  box-shadow: 0 0 12px 0px #ccc;
}


.align-item{
  display: flex;
  justify-content: center;
  align-items: center;
}


.active-attr {
  display: block !important;
}
.notactive-attr {
  display: none !important;
}





.pledge-wraper-main input[type=checkbox] {
  width: 17px;
  height: 17px;
}

.pledge-wraper-main input[type=checkbox]:checked+label::before {
  content: "";
  display: block;
  position: absolute;
  text-align: center;
  height: 19px;
  width: 19px;
  left: 0;
  top: 0;
  background-color: #5f2567;
  font-family: "Montserrat";
  border-radius: 2px;
  border: 1px solid rgb(150 150 150 / 30%);
  pointer-events: none;
}

.pledge-wraper-main input[type=checkbox]:checked+label::after {
  content: url('../img/gray-tick.svg');
  display: block;
  position: absolute;
  left: 3px;
  top: 0px;
  filter: brightness(0) invert(1);
  pointer-events: none;
}

.form-group.recognize-form {
  display: none;
}

.pledgec-confirm .pledge-confrim-inner .item-tick {
  display: flex;
  align-items: center;
  justify-content: flex-start;
}



/* Radio Button Pledge Recognized */

.pldge-rec-ul input[type="radio"]{
  display: none;
}

.radio_cus_pledg {
  opacity: 0;
  position: absolute;   
}

.radio_cus_pledg, .raio_label_pledg {
  display: inline-block;
  vertical-align: middle;
  margin: 5px;
  cursor: pointer;
}

.raio_label_pledg {
  position: relative;
}

.radio_cus_pledg + .raio_label_pledg:before {
  border: 1px solid #000 !important;
  content: '';
  background: #fff;
  border: none;
  display: inline-block;
  vertical-align: middle;
  width: 17px;
  height: 17px;
  margin-right: 10px;
  text-align: center;
  border-radius: 2px;
}


.radio_cus_pledg:checked + .raio_label_pledg:before {
  content: "\f00c";
  font-family: 'FontAwesome';
  color: #fff;
  font-size: 11px;
  background: #5f2567;
}
.pldge-rec-ul label {
  margin-left: 0 !important;
}
.form-group.recognize-form.recognize-form-active {
  display: block;
}
.pledge-wraper-main {
  transform-origin: center center;
  zoom: 0.999;
  transform: scale(0);
  height: 0;
  opacity: 0;
  transition: all 0.5s;
}
.pledge-wraper-main-active {transform: scale(1);height: auto;opacity: 1;}
.pledge-wraper-main-active-new{transform: scale(1);height: auto;opacity: 1;}


